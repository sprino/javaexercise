import java.util.*;

public class Exe_1_1_printlnStandard {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("Lambda", "Expressions", "Cool"); 
		
		System.out.println("Standard: ");
		for(int i = 0; i < strings.size(); i++) {
			System.out.println(strings.get(i));
		}

	}

}
