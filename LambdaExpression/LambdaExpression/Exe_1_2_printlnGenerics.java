import java.util.Arrays;
import java.util.List;

public class Exe_1_2_printlnGenerics {

	public static void main(String[] args) {
			List<String> strings = Arrays.asList("Lambda", "Expressions", "Cool"); 
		    
			System.out.println("Generics: ");
			for(String string : strings) {
				System.out.println("\r"+string+"\r");
			}
	}

}
