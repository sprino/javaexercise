import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class Exe_1_3_printlnIternalIteration {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("Lambda", "Expressions", "Cool");

		System.out.println("Iterazione interna: ");
		strings.forEach(new Consumer<String>() {
			public void accept(String s) {
				System.out.println(s);
			}
		});
	}

}
