import java.util.Arrays;
import java.util.List;

public class Exe_1_4_printlnInternalIterationSemplyfy_1 {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("Lambda", "Expressions", "Cool");

		System.out.println("Iterazione interna semplificata: ");
		strings.forEach((String s) -> System.out.println(s));
	}

}
