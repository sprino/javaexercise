import java.util.Arrays;
import java.util.List;

public class Exe_1_5_printlnInternalIterationSemplyfy_2 {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("Lambda", "Expressions", "Cool");

		System.out.println("Iterazione interna semplificata, meno verbosa: ");
		strings.forEach(s -> System.out.println(s));
	}

}
