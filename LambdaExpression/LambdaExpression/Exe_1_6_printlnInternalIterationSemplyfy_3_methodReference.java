import java.util.Arrays;
import java.util.List;

public class Exe_1_6_printlnInternalIterationSemplyfy_3_methodReference {

	public static void main(String[] args) {
		List<String> strings = Arrays.asList("Lambda", "Expressions", "Cool");

		System.out.println("Method reference: ");
		strings.forEach(System.out::println);
	}

}
