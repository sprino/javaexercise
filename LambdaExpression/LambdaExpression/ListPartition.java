import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class ListPartition {

	public static void main(String[] args) {
		
		MyObject object_1 = new MyObject("A", "fff", "jjj");
		MyObject object_2 = new MyObject("B", "eee", "eee");
		MyObject object_3 = new MyObject("A", "fff", "hhh");
		
		MyObject object_4 = new MyObject("A", "yyy", "yyy");
		MyObject object_5 = new MyObject("B", "fff", "kkk");
		MyObject object_6 = new MyObject("C", "fff", "ppp");
		
		MyObject object_7 = new MyObject("B", "fff", "lll");
		MyObject object_8 = new MyObject("C", "fff", "rrr");
		MyObject object_9 = new MyObject("C", "ppp", "mmm");
		MyObject object_10 = null;
		
		List<MyObject> objectList = Arrays.asList(object_1,object_2,object_3,object_4,object_5,
				object_6,object_7,object_8,object_9, object_10);

		Map<String, List<MyObject>> groups = objectList.stream().
				filter(n -> (n!=null && n.getTipo().equals("fff"))).collect(Collectors.groupingBy(MyObject::getNome));
        List<List<MyObject>> subSets = new ArrayList<List<MyObject>>(groups.values());

        subSets.forEach(m->m.forEach(s->System.out.println("->>>"+s.getNome())));
	}

} 